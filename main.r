library(wordVectors)
library(tsne)
library(Rtsne)
library(ggplot2)
library(ggrepel)
library(stringi)
library(tm)
library(magrittr)
library(stopwords)
library(readr)
library(wordcloud)
library(cowplot)

# custom eng stopwords
my_stopwords <- read_file("./stopwords.txt")
my_stopwords <- strsplit(my_stopwords, ", ") 
my_stopwords <- unlist(my_stopwords)

my_jap_stopwords <- c("japanese", "japan", "great", "long", "called")

get_assets <- function(save_to_fs = TRUE) {
  #' Get all text files of the assets folder
  #' 
  #' Read and parse all .txt files inside a directory and parse
  #' parse them into a format that can be easily analyzed
  #' @param first boolean. Write files to filesystem
  #' @return Returns a vector of "strings". Vector of the parsed .txt files
  #' @examples
  #' assets <- get_assets()
  assets_path <- "./assets/"
  assets_dir <- dir(assets_path, "\\.txt") # ["buch 1", "buch 2"]
  
  parsed_files <- c(); # init return value
  for(i in 1:length(assets_dir)) {
    # read file
    text <- scan(paste(assets_path, assets_dir[i], sep = "/"),
                 what = "character", 
                 sep = "\n")
    text <- paste(text, 
                  collapse = " ")
    text <- tolower(text)
    text <- removeNumbers(text)
    text <- removePunctuation(text)
    parsed_files <- c(parsed_files, text)
    ## TODO
    ## Write to file system
    # if (should_save == TRUE) {
    dir.create("./output/")
    dir.create("./output/texts/")
    write(text, file = sprintf("./output/texts/%d.txt", i))
    # }

  }
  return(parsed_files)
}

word_ranking <- function(books, wordcount = 10, col = "blue") {
  #' Create word ranking for many writings
  #' 
  #' Creates a plot for each writing in a vector
  #' that shows the relative frequency of n words
  #' defined by wordcount
  #' @param first  vector. Vector of strings
  #' @param second number. How many words you want to be displayed
  #' @param third  string. Color of the graph
  #' @return void.
  #' @examples
  #' assets <- get_assets()
  #' word_ranking_for_many(assets, wordcount = 10)
  for(i in 1:length(books)) {
    text <- strsplit(books[i], "\\W")
    text <- unlist(text) # convert to vec
    text <- text[!(text %in% c("", " "))]
    text <- text[!(text %in% my_stopwords)] # remove common eng words
    ## TODO
    # Get frequency of words inside a text 
    freqs <- table(text)
    sorted <- sort(freqs, decreasing = TRUE)
    # plot(sorted[1:10])
    # Get relative frequency
    rel_freqs <- 100 * (sorted / sum(sorted))
    plot(rel_freqs[1:wordcount],
         type = "b",
         main= "Word ranking",
         xlab = "Top Ten Words",
         ylab = "Percentage of Chapter",
         col = col)
  }
}

combine_books <- function(books) {
  #' Combine a vector of books (string) into on string
  #' 
  #' Verbinde alle items im assets ordner und erstelle einen rel_freq plot
  #' @param first vector. Vector of strings.
  #' @returns Combined string.
  combined <- "";
  for (i in 1:length(books)) {
    combined <- paste(combined, books[i]);
  }
  return(combined);
  # word_ranking(combined, wordcount = wordcount)
}

assets <- get_assets();
# word_ranking_for_many(assets, wordcount = 10);
# test <- paste(assets[1], assets[2], assets[3])
# word_ranking(test)
# Word ranking many in one
# word_ranking_many_in_one(assets, wordcount = 30)
word_ranking(c(combine_books(assets)), 
             wordcount = 30);

## Wenn man nur von einen Buch/Werk/Text die relative Frequency haben möchte
## kann man das wie folgt machen:
word_ranking(c(assets[2]));




## hier ist noch wordcloud drinne
word_ranking_ggplot <- function () {
  # load the corpus
  docs <- Corpus(DirSource(file.path("./assets/")))
  # Preprocess the text
  docs <- tm_map(docs, removePunctuation)
  # Remove punctuation
  docs <- tm_map(docs, removeNumbers)
  # Remove numbers
  docs <- tm_map(docs, tolower)
  # Convert to lowercase
  docs <- tm_map(docs, removeWords, my_stopwords)
  docs <- tm_map(docs, removeWords, c("japanese", "great", "japan"))
  # docs <- tm_map(docs, removeWords, stopwords("english")) # To remove stopwords
  docs <- tm_map(docs, stripWhitespace)
  # Strip whitespace
  # This is the end of the preprocessing stage.
  
  # Create a Document Term Matrix
  dtm <- DocumentTermMatrix(docs)
  # To view or export the DTM
  corpus_dtm <- as.matrix(dtm)
  rownames(corpus_dtm) <- c("PP", "MP", "CP")
  
  
  ### It is also possible to create a Term Document Matrix
  # tdm <- TermDocumentMatrix(docs)
  # corpus_tdm <- as.matrix(tdm)
  # colnames(corpus_tdm) <- c("PP", "MP")
  
  # Write files to a folder
  # write.csv(corpus_dtm, "Results/dtm.csv")
  # write.csv(corpus_tdm, "Results/tdm.csv")
  # Explore your data - this gives the raw frequency for each word in the corpus
  
  freq <- colSums(as.matrix(dtm))
  # length(freq)
  # the number of unique words in the corpus
  # head(freq) # view the top 6 elements
  
  # Create a frequency graph of the words which appear more than 500 times
  word_freq <- data.frame(word=names(freq), freq=freq)
  p <- ggplot(subset(word_freq, freq>100), aes(word, freq))
  p <- p + geom_bar(stat="identity")
  p <- p + theme(axis.text.x=element_text(angle=45, hjust=1))
  p
  # Create a wordcloud
  set.seed(142)
  # set.seed is used to ensure replicability
  wordcloud(names(freq), freq, max.words=100)
  set.seed(142)
  wordcloud(names(freq), freq, min.freq=100, scale=c(5, .1),
            colors=brewer.pal(8, "Set1")) # This section determines the colours
}

word_ranking_for_many <- function(books, wordcount) {
  #' Create word ranking for many writings
  #' 
  #' Creates a plot for each writing in a vector
  #' that shows the relative frequency of n words
  #' defined by wordcount
  #' @param first  vector. Vector of strings
  #' @param second number. How many words you want to be displayed
  #' @return void.
  #' @examples
  #' assets <- get_assets()
  #' word_ranking_for_many(assets, wordcount = 10)
  stopWords <- stopwords("en") # word list of common eng words
  master_plot <- list();
  for(i in 1:length(books)) {
    text <- strsplit(books[i], "\\W")
    text <- unlist(text) # convert to vec
    text <- text[!(text %in% c("", " "))]
    text <- text[!(text %in% stopWords)] # remove common eng words
    text <- text[!(text %in% my_stopwords)]
    text <- text[!(text %in% my_jap_stopwords)]
    # Get frequency of words inside a text
    freqs <- table(text)
    sorted <- sort(freqs, decreasing = TRUE)
    # plot(sorted[1:10])
    # Get relative frequency
    rel_freqs <- 100 * (sorted / sum(sorted))
    df <- data.frame(sorted)
    plt2 <- ggplot(df[1:10,], aes(x=as.factor(text), y=as.factor(Freq))) + 
      geom_bar(stat = "identity")
    master_plot[[length(master_plot) + 1]] <- plt2 # add item to list
    # ggarrange += plt2
    # plot(rel_freqs[1:wordcount], 
    #      type = "b", 
    #      main= "Relative frequencies in PP Ch 1",
    #      xlab = "Top Ten Words", 
    #      ylab = "Percentage of Chapter")
  }
  # return(plt2)
  return(master_plot)
}

tmp <- word_ranking_for_many(assets, 10);
plot_grid(plotlist = tmp, labels="AUTO")










## Lege Plots übereinander
par(new=TRUE)
word_ranking(assets[1])
par(new=TRUE)
word_ranking(assets[2], col = "green")
par(new=TRUE)
word_ranking(assets[3], col = "blue")
par(new=TRUE)





## ------------------------------------------ Sort of interactive way to do kwic
# Create output dir
dir.create("./output/")

prep_word2vec("./output/texts/", # dir wo alle texte genommen werden
              "./output/prep_word2vec_assets.txt",  # einfach alle zusammengeklatscht
              lowercase = TRUE,
              removeNumbers = TRUE)

ja <- train_word2vec("./output/prep_word2vec_assets.txt", # prepared file
                     output = "./output/prep_word2vec_assets.bin",
                     threads = 3,
                     vectors = 100, # the number of dimensions (default is 100)
                     window = 15,  # number of words either side of the context word
                     force=TRUE)

ja <- read.vectors("./output/prep_word2vec_assets.bin")

files <- dir("./output/texts/", "\\.txt")
input.dir <- ("./output/texts/")
show_files <- function(files){
  ### Create a function to list the files
  for(i in 1:length(files)){
    cat(i, files[i], "\n", sep = " ")
  }
}
show_files(files) # the files are listed and numbered

make_word_list <- function(files, input.dir) {
  ### Create a function to create a list of words 
  ### from each file, you will recognise
  ### the code from the previous sections
  # create an empty list for the results
  word_list <- list()
  # read in the files and process them
  for(i in 1:length(files)) {
    text <- scan(paste(input.dir, files[i], sep = "/"),
                 what = "character", sep = "\n")
    text <- paste(text, collapse = " ")
    text_lower <- tolower(text)
    text_words <- strsplit(text_lower, "\\W")
    text_words <- unlist(text_words)
    text_words <- text_words[which(text_words != "")]
    word_list[[files[i]]] <- text_words
  }
  return(word_list)
}

my_corpus <- make_word_list(files, input.dir)

my_corpus <- unlist(my_corpus) # convert to vec
my_corpus <- tm_map(my_corpus, removePunctuation)
# Remove punctuation
my_corpus <- tm_map(my_corpus, removeNumbers)
# Remove numbers
my_corpus <- tm_map(my_corpus, tolower)
my_corpus <- as.list(my_corpus)# convert back to list


kwic <- function(my_corpus) {
  ### The Novel_corpus.txt file will be read in as 2 items as it has already been
  ### processed - this doesn’t have an impact on using kwic() later.
  ### Creating a Key Word in Context (KWIC) function - this uses the output from
  ### the make_word_list and includes the show_files function
  show_files(names(my_corpus))
  # identify the chosen file
  file_id <- as.numeric(
    readline("Which file would you like to examine? Enter a number: \n"))
  # identify the number of words each side of the keyword
  context <- as.numeric(
    readline("How much context do you want? Enter a number: \n"))
  # identify the focus word
  keyword <- tolower((readline("Enter a keyword: \n")))
  # create the KWIC readout
  hits <- which(my_corpus[[file_id]] == keyword)
  if(length(hits) > 0) {
    result <- NULL
    for(j in 1:length(hits)) {
      start <- hits[j] - context
      if(start < 1) {
        start <- 1
      }
      end <- hits[j] + context
      cat("\n--------------------", j, "----------------\n")
      cat(my_corpus[[file_id]][start:(hits[j] -1)], sep = " ")
      cat("[", my_corpus[[file_id]][hits[j]], "] ", sep = " ")
      cat(my_corpus[[file_id]][(hits[j] +1): end], sep = " ")
      myrow <- cbind(hits[j],
                     paste(my_corpus[[file_id]][start: (hits[j] -1)],
                           collapse = " "),
                     paste(my_corpus[[file_id]][hits[j]],
                           collapse = " "),
                     paste(my_corpus[[file_id]][(hits[j] +1): end],
                           collapse = " "))
      result <- rbind(result, myrow)
    }
    colnames(result) <- c("position", "left", "keyword", "right")
    return(result)
  } else {
    cat("YOUR KEYWORD WAS NOT FOUND\n")
  }
}

results <- kwic(my_corpus) # using the kwic() function explore the context of some of the words in the plot
# If you wish to save your results amend this piece of code:
# If you wish to save the results as a .csv file
write.csv(results, "./output/yourkeyword_text.csv")









## ------------------------------------------ Plot
# The nearest_to() function uses cosine similarity
# The nearest to() function allows you to search the vector space model for words which
# appear near to a chosen word or set of words, both semantically and contextually. The
# selected words can be viewed as a list or a plot. It is useful at this point to explore the
# specific context any words of interest appear in by using the kwic() function.
nearest_to(ja, ja[["marriage"]], 50) # 50 nearest words to marriage
keyword <- nearest_to(ja, ja[["marriage"]], 100) # This creates a vector of distances for the 100 words nearest to marriage
plot(ja[[names(keyword), average = FALSE]]) # Create a plot of the 100 words nearest to marriage










# You can also search for words nearest to a semantic field

my_word_list <- c("temple", "shrine","god", "shinto", "foxes", "buddhist", "priest")
my_word_list <- c("family", "child", "children", "men", "woman", "household")
my_word_list <- c("foreign", "things", "work", "life", "time")
my_word_list1 <- c("supply")
my_word_list2 <- c("markets", "market")
word_region = 300 #300
sample_size = 100 #100

wealth <- nearest_to(ja, 
                     ja[[my_word_list]],
                     word_region) # n = number of closest words to include
wealth <- names(wealth)
sample(wealth, sample_size) # size	non-negative integer giving the number of items to choose.

wealth1 <- ja %>% nearest_to(ja[[my_word_list]], 
                             word_region) %>% names
data.frame(wealth1)
sample(wealth1, sample_size)
# Plot the chosen segment
plot(ja[[wealth[1:200], average = F]])

## convert to data.frame
## create ggplot
## cowplot.plot_grid append/combine all ggplots
fdsf <- function(wordlists) {
  result <- list();
  for(i in 1:length(wordlists)) {
    wealth <- nearest_to(ja, 
                         ja[[wordlists[i]]],
                         word_region) # n = number of closest words to include
    wealth <- names(wealth)
    sample(wealth, sample_size) # size	non-negative integer giving the number of items to choose.
    
    wealth1 <- ja %>% nearest_to(ja[[wordlists[i]]], 
                                 word_region) %>% names
    
    sample(wealth1, sample_size)
    # Plot the chosen segment
    x <- plot(ja[[wealth[1:100], average = F]])
    result[[length(result) + 1]] <- x # add item to list
  }
  return(result)
}

plot_grid(plotlist = fdsf(c(my_word_list1, my_word_list2)));

